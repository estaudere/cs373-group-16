from sqlalchemy import Column, Integer, String, ForeignKey, Float
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import JSONB

from database import Base, engine

class State(Base):
    __tablename__ = "states"

    id = Column(Integer, primary_key=True)
    population = Column(Integer)
    num_health_centers = Column(Integer)
    num_hospitals = Column(Integer)
    percent_uninsured = Column(Float)
    percent_low_income = Column(Float)
    image = Column(String)
    name = Column(String)
    description = Column(String)
    topojson = JSONB(String)
    counties = relationship("County", back_populates="state")
    hospitals = relationship("Hospital", back_populates="state")
    organizations = relationship("Organization", back_populates="state")

class Hospital(Base):
    __tablename__ = "hospitals"
    
    id = Column(Integer, primary_key=True)
    city = Column(String)
    state_abbr = Column(String)
    name = Column(String)
    address = Column(String)
    ownership = Column(String)
    type = Column(String)
    emergency_services = Column(String)
    rating = Column(Integer)
    map = Column(String)
    image = Column(String)
    description = Column(String)
    state = relationship("State", back_populates="hospitals")
    state_id = Column(Integer, ForeignKey("states.id"))
    organizations = relationship("Organization", secondary="hospitals_orgs", back_populates="hospitals")
    url = Column(String)


class Organization(Base):
    __tablename__ = "organizations"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    address = Column(String)
    city = Column(String)
    state_abbr = Column(String)
    description = Column(String)
    image = Column(String)
    map = Column(String)
    type = Column(String)
    state_id = Column(Integer, ForeignKey("states.id"))
    state = relationship("State", back_populates="organizations")
    hospitals = relationship("Hospital", secondary="hospitals_orgs", back_populates="organizations")
    url = Column(String)
    zip = Column(Integer)
    num_patients_2022 = Column(Integer)
    percent_200fpl = Column(Float)
    percent_100fpl = Column(Float)
    percent_uninsured = Column(Float)
    percent_medicaid_chip = Column(Float)
    percent_medicare = Column(Float)
    percent_other_insurance = Column(Float)
    services = Column(String)

class County(Base):
    __tablename__ = "counties"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    percent_uninsured = Column(Float)
    fips = Column(Integer)
    state_id = Column(Integer, ForeignKey("states.id"))
    state = relationship("State", back_populates="counties")


class Hospitals_Orgs(Base):
    __tablename__ = "hospitals_orgs"

    hospital_id = Column(Integer, ForeignKey("hospitals.id"), primary_key=True)
    organization_id = Column(Integer, ForeignKey("organizations.id"), primary_key=True)
