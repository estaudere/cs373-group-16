from requests_html import HTMLSession
from bs4 import BeautifulSoup
from readability import Document
import psycopg2
import re


headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0'}
db_host = "localhost"
db_port = "5432"
db_name = "postgres"

connection = psycopg2.connect(
            host=db_host,
            port=db_port,
            dbname=db_name
        )
cursor = connection.cursor()

import signal
def handler(signum, frame):
    connection.commit()
    cursor.close()
    connection.close()

signal.signal(signal.SIGTSTP, handler)

def fetch_hospital_url(name: str, city: str, state_abbr: str):
    query = f"{name} {city} {state_abbr}"
    search_url = f"https://www.google.com/search?q={query}"

    # Send a GET request to Google Search
    session = HTMLSession()
    response = session.get(search_url, headers=headers)
    response.html.render(sleep=1)

    # Parse HTML content using BeautifulSoup
    soup = BeautifulSoup(response.text, 'html.parser')
    # print(soup.prettify())

    # Find the first search result link
    link = soup.find('div', text=re.compile('Website'), recursive=True)
    link = link.find_parent('a')['href']

    return link

if __name__ == "__main__":
    try:
        cursor.execute("SELECT name, city, state_abbr FROM hospitals WHERE description IS NULL ORDER BY random() LIMIT 30")
        hospitals = cursor.fetchall()

        while hospitals:
            for i, hospital in enumerate(hospitals):
                name, city, state_abbr = hospital
                try: 
                    link = fetch_hospital_url(name, city, state_abbr)
                except Exception as e:
                    print(f"{i}/{len(hospitals)}: Failed to fetch {name} in {city}, {state_abbr}")
                    continue
                if not link:
                    print(f"{i}/{len(hospitals)}: No link for {name} in {city}, {state_abbr}")
                    continue
                cursor.execute("UPDATE hospitals SET url = %s WHERE name = %s AND city = %s AND state_abbr = %s", (link, name, city, state_abbr))
                print(f"{i}/{len(hospitals)}: Updated {name} in {city}, {state_abbr}")
            connection.commit()
            cursor.execute("SELECT name, city, state_abbr FROM hospitals WHERE description IS NULL ORDER BY random() LIMIT 30")
            hospitals = cursor.fetchall()
          
        cursor.close()
        connection.close()
    except KeyboardInterrupt:
        connection.commit()
        cursor.close()
        connection.close()