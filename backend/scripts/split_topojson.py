import geopandas as gpd
import psycopg2
import json
import os

FIPS_TO_STATE = ['Alabama', 'Alaska', '', 'Arizona', 'Arkansas', 'California', '', 'Colorado',
                 'Connecticut', 'Delaware', 'District of Columbia', 'Florida',
                 'Georgia', '', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa',
                 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland',
                 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi',
                 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
                 'New Jersey', 'New Mexico', 'New York', 'North Carolina',
                 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', '', 
                 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee',
                 'Texas', 'Utah', 'Vermont', 'Virginia', '', 'Washington',
                 'West Virginia', 'Wisconsin', 'Wyoming']

def filter_counties_by_state_and_save_to_postgres(input_topojson, db_connection_string):
    gdf = gpd.read_file(input_topojson, driver='TopoJSON')

    conn = psycopg2.connect(db_connection_string)
    cur = conn.cursor()

    for state_fips in gdf['id'].str[:2].unique():
        state_gdf = gdf[gdf['id'].str[:2] == state_fips]
        geojson_string = state_gdf.to_json()
        geojson_dict = json.loads(geojson_string)
        jsonb_data = json.dumps(geojson_dict)
        jsonb_data = jsonb_data.replace("'", "''")
        try:
            state_name = FIPS_TO_STATE[int(state_fips) - 1]
        except IndexError:
            print(f"State FIPS {state_fips} not found in FIPS_TO_STATE list.")
            continue
        query = f"UPDATE states SET topojson = '{jsonb_data}'::jsonb WHERE name = '{state_name}';"
        cur.execute(query)

        print(f"Filtered state FIPS {state_fips} and saved to PostgreSQL database.")

    # Close the database connection
    conn.commit()
    cur.close()
    conn.close()

def main():
    input_topojson = '~/Downloads/counties-albers-10m.json'
    db_connection_string = 'postgresql://localhost:5432/postgres'

    # Call the function to filter counties by state and save TopoJSON to PostgreSQL
    filter_counties_by_state_and_save_to_postgres(input_topojson, db_connection_string)

    print("Processing completed.")

if __name__ == "__main__":
    main()
