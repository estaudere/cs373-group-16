import {expect, describe, test, it} from 'vitest';
import {render} from '@testing-library/svelte';
import DeveloperCard from "../components/DeveloperCard.svelte";
import HospitalCard from  '../components/HospitalCard.svelte';
import OrgCard from '../components/OrgCard.svelte';
import StateCard from "../components/StateCard.svelte";
import ToolCard from "../components/ToolCard.svelte";

describe('Developer Card component', () => {

    // test 1
    it("checks developer card using name", ()=> {
        const props = {
            name: 'John Doe',
            role: 'Frontend Developer',
            description: 'Lorem ipsum dolor sit amet',
            imageUrl: '/path/to/image.jpg',
            commits: 10,
            issues: 5
        };

        // Render the DeveloperCard component with the defined props
        const {getByText} = render(DeveloperCard, {props});

        // Assert that the component renders with the correct props
        expect(getByText('John Doe')).toBeInTheDocument();
    });

    // test 2
    it("checks developer card using description", ()=> {
        const props = {
            name: 'John Doe',
            role: 'Frontend Developer',
            description: 'Lorem ipsum dolor sit amet',
            imageUrl: '/path/to/image.jpg',
            commits: 10,
            issues: 5
        };

        // Render the DeveloperCard component with the defined props
        const {getByText} = render(DeveloperCard, {props});

        // Assert that the component renders with the correct props
        expect(getByText('Lorem ipsum dolor sit amet')).toBeInTheDocument();
    });
});

describe('Hospital Card component', () => {

    // test 3
    it("checks hospital card using name", ()=> {
        const props = {
            name: 'SANFORD MAYVILLE',
            city: 'MAYVILLE'
        };

        // Render the HospitalCard component with the defined props
        const {getByText} = render(HospitalCard, {props});

        // Assert that the component renders with the correct props
        expect(getByText('SANFORD MAYVILLE')).toBeInTheDocument();
    });

    // test 4
    it("checks hospital card using different name", ()=> {
        const props = {
            name: 'CLAY COUNTY HOSPITAL',
            city: 'ASHLAND'
        };

        // Render the HospitalCard component with the defined props
        const {getByText} = render(HospitalCard, {props});

        // Assert that the component renders with the correct props
        expect(getByText('CLAY COUNTY HOSPITAL')).toBeInTheDocument();
    });

    // test 5
    it("checks hospital card using different name", ()=> {
        const props = {
            name: 'WIREGRASS MEDICAL CENTER',
            city: 'GENEVA'
        };

        // Render the HospitalCard component with the defined props
        const {getByText} = render(HospitalCard, {props});

        // Assert that the component renders with the correct props
        expect(getByText('WIREGRASS MEDICAL CENTER')).toBeInTheDocument();
    });
});

describe('Org Card component', () => {

    // test 6
    test("checks org card using name", async ()=> {
        const props = {
            name: 'University of Kentucky',
        };

        // Render the OrgCard component with the defined props
        const {getByText} = render(OrgCard, {props});

        // Assert that the component renders with the correct props
        await expect(getByText('University of Kentucky')).toBeInTheDocument();
    });

    // test 7
    test("checks org card using different name", async ()=> {
        const props = {
            name: 'NORTHWEST COMMUNITY HEALTH CARE',
        };

        // Render the OrgCard component with the defined props
        const {getByText} = render(OrgCard, {props});

        // Assert that the component renders with the correct props
        await expect(getByText('NORTHWEST COMMUNITY HEALTH CARE')).toBeInTheDocument();
    });
});

describe('State Card component', () => {
    // test 8
    test("checks state card using name", ()=> {
        const props = {
            name: 'Texas',
        };

        // Render the StateCard component with the defined props
        const {getByText} = render(StateCard, {props});

        // Assert that the component renders with the correct props
        expect(getByText('Texas')).toBeInTheDocument();
    }); 

    // test 9
    test("checks state card using different name", ()=> {
        const props = {
            name: 'New Jersey',
        };

        // Render the StateCard component with the defined props
        const {getByText} = render(StateCard, {props});

        // Assert that the component renders with the correct props
        expect(getByText('New Jersey')).toBeInTheDocument();
    }); 

    // test 10
    test("checks state card using different name", ()=> {
        const props = {
            name: 'California',
        };

        // Render the StateCard component with the defined props
        const {getByText} = render(StateCard, {props});

        // Assert that the component renders with the correct props
        expect(getByText('California')).toBeInTheDocument();
    }); 
});

// describe('Tool Card component', () => {
//     // test 9
//     it("renders tool card with correct name", ()=> {
//         const props = {
//             imageUrl: '/path/to/image.png',
//             name: 'John Doe'
//         };
    
//         // Render the ToolCard component with the defined props
//         const { getByText } = render(ToolCard, { props });
    
//         // Assert that the component renders with the correct props
//         expect(getByText('John Doe')).toBeInTheDocument();
//     });

//     // test 10
//     it("renders tool card with correct name", async ()=> {
//         const props = {
//             imageUrl: '/path/to/image.png',
//             name: 'Svelte'
//         };

//         // Render the ToolCard component with the defined props
//         const { getByText } = await render(ToolCard, { props });
//         const toolName = getByText(props.name);
//         // Assert that the component renders with the correct props
//         expect(toolName).toBeDefined();
//     });
// });




