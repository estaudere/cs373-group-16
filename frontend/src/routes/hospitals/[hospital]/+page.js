export const prerender = false;

export async function load({fetch, params}) {
  const res = await fetch(`https://api.care-hub.me/hospitals/` + params.hospital);
  const data = await res.json();
  if (data) {
    return {
      hospital: data
    }
  } else {
    return {
      status: 404
    }
  }
}