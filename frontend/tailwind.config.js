/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{html,js,svelte}'],
  theme: {
    extend: {
      colors: {
        'light-blue': '#8eb5bf',
      }
    },
  },
  plugins: [require("daisyui")],
  daisyui: {
    themes: ['pastel'],
  }
}

